# dodgebandicoot

This is the Dodge Bandicoot's repository, a Crash Bandicoot remake that is a platform game, where the "leading actor" is an unknown Crash's brother.
He has to (exactly) dodge the obstacles and he has to get the power ups to increase his health.

Development team: Alessandro Sabatino, Andrea Pecorari, Leonardo Perugini (Faculty of Engineering and Computer Science, University of Bologna)
