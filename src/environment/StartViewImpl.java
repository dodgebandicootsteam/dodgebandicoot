package environment;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.event.ActionEvent;


public class StartViewImpl extends Application {
	
	@FXML
	public void doButtonStart(ActionEvent event) throws Exception {
	     try {
	    	 Stage stage = new Stage();
	    	 GameViewImpl gameView=new GameViewImpl();
	    	 gameView.start(stage);
	     } catch (NumberFormatException e) {
	         e.printStackTrace();
	     }
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			VBox root = FXMLLoader.load(getClass().getResource("/environment/StartViewFX.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setFullScreen(true);
			
			primaryStage.show(); 
		} catch(Exception e) {
			e.printStackTrace();
		}
				
	}
	
	public static void main(String[] args) {
		launch(args);

	}


}
