package environment;

import javafx.event.ActionEvent;

public interface StartView {
	
	void doButtonStart(ActionEvent event);

}
