package environment;
public interface Input {

    public void addListeners();
    public void removeListeners();

    /** 
     * Evaluate bitset of pressed keys and return the player input.
     */
    public boolean isMove(String where);
	
}
