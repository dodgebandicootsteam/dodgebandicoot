package environment;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.imageio.ImageIO;

import javafx.animation.Animation;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class GameViewImpl extends Application implements GameView{

    private static final int COLUMNS  =   4;
    private static final int COUNT    =  10;
    private static final int OFFSET_X =  18;
    private static final int OFFSET_Y =  25;
    private static final int WIDTH    = 374;
    private static final int HEIGHT   = 243;
    
    @FXML
	public void moveStart(ActionEvent event) throws Exception {
    	 final ImageView imageView = new ImageView("images//Henchman.gif");
         imageView.setViewport(new Rectangle2D(OFFSET_X, OFFSET_Y, WIDTH, HEIGHT));

         final Animation animation = new SpriteAnimation(
                 imageView,
                 Duration.millis(1000),
                 COUNT, COLUMNS,
                 OFFSET_X, OFFSET_Y,
                 WIDTH, HEIGHT
         );
         animation.setCycleCount(Animation.INDEFINITE);
         animation.play();

	}
    
	@Override
	public void start(Stage primaryStage){
		try {
			VBox root = FXMLLoader.load(getClass().getResource("/environment/GameViewFX.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("game.css").toExternalForm());
			primaryStage.setScene(scene);
			
			primaryStage.setFullScreen(true);
			primaryStage.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
				
	}
	 
	@Override
	public void setObserver() {
		// TODO Auto-generated method stub
		
	}
	
	 public static void main(String[] args) {
			launch(args);

		}
	
	
}
