package environment;

import javafx.event.ActionEvent;
import javafx.stage.Stage;

public interface GameView {
	
void setObserver();
	
void moveStart(ActionEvent event) throws Exception;

void start(Stage primaryStage);

}
