package characters;

public class Position {

	private int x;
	private int y;

	public Position(Position p) {
		this.x = c.getX();
		this.y = c.getY();
	}
	
	public Position(final int x, final int y) {
		this(new Position(x,y));
	}

	public Position() {
		this(0, 0);
	}

	public int getX() {
		return Integer.valueOf(this.x);
	}

	public int getY() {
		return Integer.valueOf(this.y);
	}

}
