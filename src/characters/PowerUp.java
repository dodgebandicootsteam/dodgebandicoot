package characters;

public interface PowerUp {

	public Position getPosition();
	
	public void boost();
	
	
}
