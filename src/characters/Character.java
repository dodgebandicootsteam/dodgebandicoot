package characters;

public interface Character {
	
	void moveLeft();
	
	void moveRight();
	
	int getPosition();

}
